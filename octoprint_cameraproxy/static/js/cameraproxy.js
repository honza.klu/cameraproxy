$(function () {
    var intervalId = setInterval(function () {
        $.ajax({
            url: API_BASEURL + "plugin/cameraproxy",
            //get_picture
            type: "POST",
            dataType: "json",
            data: JSON.stringify({
                command: "get_picture"
            }),
            contentType: "application/json; charset=UTF-8"
        }).done(function (data) {
            //window.alert("asdf");
            $("#webcam_proxy_img").attr("src", "data:image/jpeg;base64," + data["picture"])
            //window.alert("ASDF");
        });
    }, 1000);
});