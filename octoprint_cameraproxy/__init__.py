from __future__ import absolute_import, unicode_literals

import requests

import base64
import flask

import octoprint.plugin

__plugin_name__ = "Camera proxy"
__plugin_version__ = "0.0.1"
__plugin_description__ = "Octoprint plugin that allow acces webcam from same address as Octoprint web interface instead its original address."
__plugin_pythoncompat__ = ">=3.6,<4"

class CameraProxyPlugin(octoprint.plugin.StartupPlugin,
                        octoprint.plugin.TemplatePlugin,
                        octoprint.plugin.SettingsPlugin,
                        octoprint.plugin.SimpleApiPlugin,
                        octoprint.plugin.AssetPlugin,
                        ):

    def on_after_startup(self):
        self._logger.info("Hello World! (more: %s)" % self._settings.get(["url"]))

    def get_settings_defaults(self):
        return dict(url="https://en.wikipedia.org/wiki/Hello_world")

    def get_template_vars(self):
        return dict(url=self._settings.get(["url"]), debug=self._settings.global_get(["webcam", "snapshot"]))

    def get_api_commands(self):
        return dict(
            get_picture=[],
            command2=["some_parameter"]
        )

    def get_assets(self):
        return dict(
            js=["js/cameraproxy.js"],
        )

    def on_api_command(self, command, data):
        if command == "get_picture":
            snapshot_path = self._settings.global_get(["webcam", "snapshot"])
            r = requests.get(snapshot_path)
            if r.status_code!=200:
                raise ValueError("Picture download failed")
            d = base64.b64encode(r.content)
            return flask.jsonify(picture=d)
        else:
            raise ValueError("Unknown command %s" % command)

__plugin_implementation__ = CameraProxyPlugin()